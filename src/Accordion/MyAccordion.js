import React, { useState } from 'react'
import questions from './Api';

const MyAccordion = ({question, answer}) => {
    console.log(question);

    const [show, setShow] = useState(false);

    return(
        <>  
            <div className="part">
                <p className="sign" onClick={() => setShow(!show)}>{show ? "-" : "+" }</p>
                <h3 className="question">{question}</h3>
            </div>
            {show && <p className="answer">{answer}</p>}    
        </>
    )
}

export default MyAccordion