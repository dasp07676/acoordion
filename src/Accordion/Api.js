const questions = [
    {
        id : 1,
        question : "What is your name?",
        answer : "My name is Pritam Das...I am from Kolkata, I am learing React now so that I can get a job at a IT sector as a Senior Software developer..."
    },
    {
        id : 2,
        question : "What is your Pet's name?",
        answer : "Well, I don't have a pet, but I would love to have one...I want a Rabit!!!"
    },
    {
        id : 3,
        question : "What is your real name?",
        answer : "My real name is Pritam Das..."
    },
    {
        id : 4,
        question : "What is your passion?",
        answer : "I like to sing, play guitar...That's what I don when I get free time..."
    },
    {
        id : 5,
        question : "What do u do at home?",
        answer : "Mostly Coding!!!"
    }
]

export default questions;