import React, { useState } from 'react'
import questions from './Api'
import MyAccordion from './MyAccordion'

const Func = () => {
    const [data] = useState(questions)
    return(
        <>
            <section className="main-div">
                <h1 className="header">A Simple Accordion</h1>
                {
                    data.map((data) => {
                        return <MyAccordion key={data.id} {...data}/>
                    })
                }
            </section>
        </>
    )
}

export default Func;